---
layout: handbook-page-toc
title: "Marketing Finance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Marketing Finance Handbook!

## Common Links
 * [Financial Planning & Analysis (FP&A)](https://about.gitlab.com/handbook/finance/financial-planning-and-analysis/)
 * [Accounting](https://about.gitlab.com/handbook/finance/accounting/)
 * [Allocadia (Marketing Financial Planning tool)](https://about.gitlab.com/handbook/marketing/strategy-performance/allocadia/)


## Finance Business Partner Alignment

| Name | Function |
| -------- | ---- |
| @cmachado1 | Sr. Manager |
| @cmcclure1 | Financial Analyst |


## Marketing Variance Packages

The Marketing Variance Review meeting happens once a month to review the actual expenses for the previous month. For example, in the 3rd week of June we will review May's expenses.  

## Important Dates
* 2021-06-24: June Forecast Due
* 2021-07-14: June Actuals loaded into Allocadia (estimated) 
* 2021-07-23: July Forecast Due 
* 2021-07-30: Q3 Plan Due 
* 2021-08-13: July Actuals loaded into Allocadia (estimated) 
* 2021-08-25: August Forecast Due 
* 2021-09-14: August Actuals loaded into Allocadia (estimated) 
* 2021-09-24: September Forecast Due 
* 2021-10-14: September  Actuals loaded into Allocadia (estimated) 
* 2021-10-22: October Forecast Due 
* 2021-11-12: October Actuals loaded into Allocadia (estimated) 
* 2021-10-29: Q4 Plan Due 
* 2021-11-19: FY23 Bottom's Up Plan due in Allocadia
* 2021-11-24: November Forecast Due 
* 2021-12-14: November Actuals loaded into Allocadia (estimated)
* 2021-12-24: December Forecast Due 
* 2022-01-14: December Actuals loaded into Allocadia (estimated) 
* 2022-01-25: January Forecast Due 
* 2022-02-14: January Actuals loaded into Allocadia (estimated)

## Finance Terminology

* Budget - the amount of money that is given at the start of the fiscal year
* Plan - locked at the start of the quarter and is your best itemized guess at what you will spend
* Forecast - never locked and you will continually update Allocadia through each quarter with accurate expenses as you know more

## Marketing Budget Holders

The marketing budget holder should be updating their forecast throughout the month in Allocadia as expenses occur and as they have more insight into the spend. On the Forecast Due dates listed above, Finance will take what is in Allocadia and compare that against Actuals in the Actuals vs Budget (AvB) file. When discrepancies greater than $1000 occur between forecast and actuals, accounting and finance will check with the budget holder to see if an accrual needs to occur.

At the plan due dates above, finance will take what is loaded into Allocadia for the quarter and add that into Adaptive. This is considered your plan and you will be held accountable to plan for the remainder of the quarter. Large variances from the plan will be explained at the monthly variance review meetings with CMO staff. If the department is under-spend, the difference may be reallocated to other departments. 

## Accounting Prepaid Policy

Please see the [Accounting](https://about.gitlab.com/handbook/finance/accounting/#prepaid-expense-policy) handbook page for the latest guidance on accruals and variance. As our company grows, the guidance is subject to change 

## Budget Reallocation

You must spend your budget in the same quarter that it was originally allocated. You cannot transfer budget between quarters. 

If you have budget that you do not plan to spend in the quarter, you may reallocate these dollars to another Marketing department to spend within the same fiscal quarter. When this occurs, please get CMO approval, talk with your finance business partner, and then request a budget transfer in Allocadia. 

## Sponsorships

If you bring in revenue through sponsorships during a GitLab event, please do the following: 
1. Confirm that the campaign tag for the event has an ISO date as the first eight characters. This will signal to our accounting team to recognize the sponsorships in the correct period 
2. Create an issue outlining the sponsors, sponsors contact information and the expected sponsorship amounts. Tag finance and accounting in this issue. 
3. Work with Legal to create a sponsorship contract that can be used to send out to all of sponsors. Get these contracts signed and an agreement in place.
4. Work with your accounting partner to send out invoices to the sponsors and to ensure that the revenue is being properly accounted for. 


